# art365/resource

#### 介绍

整体用来存放静态资源

1. blog：个人博客静态资源存放位置 
2. xxx：xxx


#### 使用说明

1.  推送图片到仓库【git push [本仓库地址] master】
2.  博文中使用图片地址【https://art365.gitee.io/resource/blog/资源详细位置】
3.  使用前需要更新：【服务】--【gitee pages】--【更新】
4.  博客临时主页【https://art365.gitee.io/】

#### 使用举例

``` bash
E:\resource>git clone https://gitee.com/art365/resource.git
Cloning into 'resource'...
remote: Enumerating objects: 66, done.
remote: Counting objects: 100% (66/66), done.
remote: Compressing objects: 100% (48/48), done.
Receiving objects: 100% (66/66), 620.14 KiB | 1.08 MiB/s, done.ceiving objects:  75% (50/66), 548.00 KiB | 1.04 MiB/s

Resolving deltas: 100% (2/2), done.

E:\resource>cd ./resource

E:\resource\resource>git remote -v
origin  https://gitee.com/art365/resource.git (fetch)
origin  https://gitee.com/art365/resource.git (push)

E:\resource\resource>git add blog

E:\resource\resource>git commit -m "update image" blog
[master 1122b5e] update image
 1 file changed, 0 insertions(+), 0 deletions(-)
 rename blog/image/easy-excel/{easy-excel0.png => easy-excel-0.png} (100%)

E:\resource\resource>git push origin master
Enumerating objects: 9, done.
Counting objects: 100% (9/9), done.
Delta compression using up to 8 threads
Compressing objects: 100% (4/4), done.
Writing objects: 100% (5/5), 527 bytes | 527.00 KiB/s, done.
Total 5 (delta 1), reused 0 (delta 0), pack-reused 0
remote: Powered by GITEE.COM [GNK-5.0]
To https://gitee.com/art365/resource.git
   e2cdf92..1122b5e  master -> master

E:\resource\resource>git pull origin master

```