package com.app.controller;

import com.app.bean.User;
import io.swagger.annotations.*;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("user")
@Api("用户服务相关接口")
public class UserController {

    @GetMapping("findAll")
    @ApiOperation(value = "查询所有用户的接口", notes = "<span stype='color:red;'>用于查询所有用户信息</span>")
    public Map<Integer, String> findAll() {
        System.out.println("查所有");
        Map<Integer, String> map = new HashMap<>();
        map.put(1, "小红");
        map.put(2, "小花");
        return map;
    }

    @PostMapping("login")
    @ApiOperation(value = "用户登录", notes = "用于登录时的接口")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "用户id", dataType = "String", defaultValue = "-1"),
            @ApiImplicitParam(name = "name", value = "用户姓名", dataType = "String", defaultValue = "默认用户")
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "请求成功"),
            @ApiResponse(code = 400, message = "请求失败，路径找不到")
    })
    public String login(String id, String name) {
        System.out.println(name + "登录了");
        return "登录成功";
    }

    @PostMapping("delete/{id}/{name}")
    @ApiOperation(value = "删除用户", notes = "用于删除用户的接口")
    @ApiImplicitParams({//restful风格
            @ApiImplicitParam(name = "id", value = "用户id", dataType = "String", paramType = "path"),
            @ApiImplicitParam(name = "name", value = "用户姓名", dataType = "String", paramType = "path")
    })
    public boolean delete(@PathVariable("id") String id, @PathVariable("name") String name) {

        System.out.println(id + "  " + name + "将被删除");
        return true;
    }

    @PostMapping("update")
    @ApiOperation(value = "更新用户", notes = "用于更新用户的接口")
//    @ApiImplicitParams({//restful风格
//            @ApiImplicitParam(name = "id", value = "用户id", dataType = "String",paramType="body"),
//            @ApiImplicitParam(name = "name", value = "用户姓名", dataType = "String",paramType="body")
//    })
    public boolean update(User user) {//可省略上面注释的
        System.out.println(user.getName() + " 已更新");
        return true;
    }
}

