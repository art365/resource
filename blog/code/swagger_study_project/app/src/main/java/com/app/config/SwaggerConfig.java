package com.app.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Swagger配置类，该类里面的格式大体固定，
 * swagger可设置文档的信息，如文档的标题，副标题，公司名，接口相关开发人员的基本信息等
 */
@Configuration  //spring托管
@EnableSwagger2 //开启swagger注解扫描
public class SwaggerConfig {
    /**
     * 创建API应用
     * apiInfo() 增加API相关信息
     * 通过select()函数返回一个ApiSelectorBuilder实例,用来控制哪些接口暴露给Swagger来展现，
     * 本例采用指定扫描的包路径来定义指定要建立API的目录。
     *
     * @return Docket 接口文档
     */
    @Bean
    public Docket createRestApi() {
        //版本类型是swagger2
        return new Docket(DocumentationType.SWAGGER_2)  //表示使用swagger2的规范
                .pathMapping("/")
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.app.controller"))//扫描该包下面的API注解，控制器层的
                .paths(PathSelectors.any())
                .build()
                .apiInfo(apiInfo());
    }

    /**
     * 自定义方法apiInfo
     * 创建该API的基本信息（这些基本信息会展现在文档页面中）
     *
     * @return ApiInfo
     */
    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("Swagger学习")//文档的大标题
                .description("SpringBoot整合Swagger!")//API的描述
                .version("1.0")           //当前文档指定的版本
                .contact(new Contact("小小", "http://www.小小.cn", "123@163.com"))//开发人员的一些信息（姓名，博客主页，邮箱）
                .license("我的 license 规范")   //接口的license规范和链接，可有可无
                .licenseUrl("http://xxx2.com")
                .termsOfServiceUrl("www.baidu.com")  //Updates the terms of service url
                .build();
    }
}